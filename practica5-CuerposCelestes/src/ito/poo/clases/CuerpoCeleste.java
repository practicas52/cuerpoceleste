package ito.poo.clases;
import java.util.HashSet;

public class CuerpoCeleste {
	private String nombrar=  " ";
	private HashSet<Ubicacion> localizacion;
	private String composic=  " ";
	
	public CuerpoCeleste() {
		super();
	}
	
	public CuerpoCeleste(String nombrar, HashSet<Ubicacion> localizacion, String composic) {
		super();
		this.nombrar = nombrar;
		this.localizacion = localizacion;
		this.composic = composic;
	}
	
	public float desplazamiento(int i, int j  ) {	
	float desplazamiento=0f;
	float op=0;
	desplazamiento=(float) (Math.sqrt(Math.pow(i,2)+Math.pow(j, 2)));
	if(desplazamiento==0)
		System.out.println("-1");
	else
		System.out.println("El desplazamiento es de:"+desplazamiento);
			return (float) desplazamiento;
	}

	public float desplazamiento(int j ) {
	float  desplazamiento=j;
	return desplazamiento;
	}

	public HashSet<Ubicacion> getLocalizacion() {
		return this.localizacion;
	}

	public void setLocalizacion(HashSet<Ubicacion> localizacion) {
		this.localizacion = localizacion;
	}

	public String getComposic() {
		return composic;
	}

	public void setComposicion(String composic) {
		this.composic = composic;
	}

	public String getNombrar() {
		return nombrar;
	}
	public String toString() {
		return "CuerpoCeleste [nombre=" + nombrar + ", localizacion=" + localizacion + ", composicion=" + composic + "]";
	}

}
