package ito.poo.clases;
import java.util.HashSet;

public class Ubicacion {
	
	private String periodo=  " ";
	private float dist= 0F;
	private float longi= 0F;
	private float latit= 0F;
	
	private HashSet<Ubicacion> localizacion;

	public Ubicacion() {
		super();
	}
	
	public Ubicacion(float longi, float latit, String periodo, float dist) {
		super();
		this.longi = longi;
		this.latit = latit;
		this.periodo = periodo;
		this.dist = dist;
	}

	public float getDist() {
		return dist;
	}

	public void setDist(float dist) {
		this.dist = dist;
	}

	public float getLongi() {
		return this.longi;
	}

	public float setLongi() {
		return this.longi;
	}

	public float getLatit() {
		return this.latit;
	}
	
	public float setLatit() {
		return this.latit;
	}

	public String getPeriodo() {
		return periodo;
	}

	public String toString() {
		return "Ubicacion [longitud=" + longi + ", latitud=" + latit + ", periodo=" + periodo + ", distancia=" + dist + "]";
	}

}
